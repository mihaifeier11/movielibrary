import unittest
from Multime import Multime
from Movie import Movie
from Iterator import Iterator

class TestMultime(unittest.TestCase):
    def testAdd(self):
        multime = Multime(10)
        iterator = Iterator(multime, 10)
        movie = Movie("nume", 9.9, "Descriere", "Gen")
        multime.add(movie)
        self.assertEqual(multime.size(), 1)
        iterator.next()
        self.assertEqual(iterator.current().getNume(), "nume")
        self.assertEqual(iterator.current().getRating(), 9.9)
        self.assertEqual(iterator.current().getDescriere(), "Descriere")
        self.assertEqual(iterator.current().getGen(), "Gen")

    def testRemove(self):
        multime = Multime(10)
        movie = Movie("nume", 9.9, "Descriere", "Gen")
        multime.add(movie)
        self.assertEqual(multime.size(), 1)
        multime.remove("nume")
        self.assertEqual(multime.size(), 0)

    def testSearch(self):
        multime = Multime(10)
        movie = Movie("nume", 9.9, "Descriere", "Gen")
        self.assertEqual(multime.search("nume"), False)
        multime.add(movie)
        self.assertEqual(multime.search("nume"), True)

    def testSize(self):
        multime = Multime(10)
        movie = Movie("nume", 9.9, "Descriere", "Gen")
        self.assertEqual(multime.size(), 0)
        multime.add(movie)
        self.assertEqual(multime.size(), 1)

    def testIsEmpty(self):
        multime = Multime(10)
        movie = Movie("nume", 9.9, "Descriere", "Gen")
        self.assertEqual(multime.isEmpty(), True)
        multime.add(movie)
        self.assertEqual(multime.isEmpty(), False)

