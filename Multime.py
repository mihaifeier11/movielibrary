from TabelaDispersie import TabelaDispersie

class Multime:
    def __init__(self, numarMaxim):
        self.tabela = TabelaDispersie(numarMaxim)

    def add(self, movie):
        if (self.tabela.search(movie.getNume()) == None):
            self.tabela.add(movie)

    def remove(self, nume):
        self.tabela.remove(nume)

    def search(self, nume):
        return (not self.tabela.search(nume) == None)

    def size(self):
        return self.tabela.size()

    def isEmpty(self):
        return self.tabela.isEmpty()

