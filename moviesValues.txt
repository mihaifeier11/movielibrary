Solo: A Star Wars Story;7.2;During an adventure into the criminal underworld, Han Solo meets his future copilot Chewbacca and encounters Lando Calrissian years before joining the Rebellion.;Action
Avengers: Infinity War;8.8;The Avengers and their allies must be willing to sacrifice all in an attempt to defeat the powerful Thanos before his blitz of devastation and ruin puts an end to the universe.;Action
Black Panther;7.5;T'Challa, the King of Wakanda, rises to the throne in the isolated, technologically advanced African nation, but his claim is challenged by a vengeful outsider who was a childhood victim of T'Challa's father's mistake.;Action
Deadpool 2;8.1;Foul-mouthed mutant mercenary Wade Wilson (AKA. Deadpool), brings together a team of fellow mutant rogues to protect a young boy with supernatural abilities from the brutal, time-traveling cyborg, Cable.;Action
Annihilation;7;A biologist signs up for a dangerous, secret expedition into a mysterious zone where the laws of nature don't apply.;Adventure
Ocean's Eight;5.8;Debbie Ocean gathers an all-female crew to attempt an impossible heist at New York City's yearly Met Gala.;Comedy
Ready Player One;7.8;When the creator of a virtual reality world called the OASIS dies, he releases a video in which he challenges all OASIS users to find his Easter Egg, which will give the finder his fortune.;Adventure
Incredibles 2;9;Bob Parr (Mr. Incredible) is left to care for Jack-Jack while Helen (Elastigirl) is out saving the world.;Animation
Jurassic World: Fallen Kingdom;7;When the island's dormant volcano begins roaring to life, Owen and Claire mount a campaign to rescue the remaining dinosaurs from this extinction-level event.;Action
A Quiet Place;7.9;In a post-apocalyptic world, a family is forced to live in silence while hiding from monsters with ultra-sensitive hearing.;Horror
