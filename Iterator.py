from TabelaDispersie import TabelaDispersie
from Multime import Multime


class Iterator:
    def __init__(self, multime, numarMaxim):
        self.__numarMaxim = numarMaxim
        self.__multime = multime
        self.__current = 0

    def isValid(self):
        if self.__current < self.__numarMaxim:
            return True
        return False

    def current(self):
        return self.__multime.tabela.tabela[self.__current]

    def next(self):
        self.__current += 1

    def reset(self):
        self.__current = 0