from Multime import Multime
from Movie import Movie
from Iterator import Iterator

class Service:
    def __init__(self, numarMaxim):
        self.__multime = Multime(numarMaxim)
        self.__iterator = Iterator(self.__multime, numarMaxim)

    def add(self, nume, rating, descriere, gen):
        movie = Movie(nume, rating, descriere, gen)
        self.__multime.add(movie)

    def remove(self, nume):
        self.__multime.remove(nume)

    def search(self, nume):
        if (self.__multime.search(nume) == False):
            return False

        else:
            self.__iterator.reset()
            while (self.__iterator.isValid()):
                if (self.__iterator.current() != None and self.__iterator.current().getNume() == nume):
                    return self.__iterator.current()
                self.__iterator.next()

    def filterRating(self, rating):
        self.__iterator.reset()
        l = []
        while (self.__iterator.isValid()):
            if (self.__iterator.current() != None and self.__iterator.current().getRating() >= rating):
                l.append(self.__iterator.current())
            self.__iterator.next()
        return l

    def filterGen(self, gen):
        self.__iterator.reset()
        l = []
        while (self.__iterator.isValid()):
            if (self.__iterator.current() != None and self.__iterator.current().getGen() == gen):
                l.append(self.__iterator.current())
            self.__iterator.next()
        return l

    def readFromFile(self):
        file = open("movies.txt", "r")

        for line in file:
            line = line.split(";")
            self.add(line[0], float(line[1]), line[2], line[3])

        file.close()

    def writeToFile(self):
        file = open("movies.txt", "w")
        self.__iterator.reset()
        while (self.__iterator.isValid()):
            movie = self.__iterator.current()
            if (movie != None):
                file.write(movie.getNume() + ";" + str(movie.getRating()) + ";" + movie.getDescriere() + ";" + movie.getGen())
            self.__iterator.next()

        file.close()