class Movie:
    def __init__(self, nume, rating, descriere, gen):
        self.__nume = nume
        self.__rating = rating
        self.__descriere = descriere
        self.__gen = gen

    def getNume(self):
        return self.__nume

    def getRating(self):
        return self.__rating

    def getDescriere(self):
        return self.__descriere

    def getGen(self):
        return self.__gen

