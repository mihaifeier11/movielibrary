from Movie import Movie



class TabelaDispersie:
    def __init__(self, numarMaxim):
        self.__numarMaxim = numarMaxim
        self.tabela = [None] * self.__numarMaxim
        self.numarElemente = 0

    def hashCode(self, nume):
        litera = nume[-1]
        litera = ord(litera)
        return litera % self.__numarMaxim

    def search(self, nume):
        hash = self.hashCode(nume)
        i = hash
        j = 1
        while (i <= self.__numarMaxim+hash):
            if self.tabela[i % self.__numarMaxim] != None and self.tabela[i % self.__numarMaxim].getNume() == nume:
                    return self.tabela[i % self.__numarMaxim]
            i = (i + j * j)
            j += 1


        return None

    def isFull(self):
        if self.__numarMaxim > self.numarElemente:
            return False
        else:
            return True

    def nextEmpty(self, current):
        key = current
        j = 1
        while (self.tabela[current % self.__numarMaxim] != None):
            current = (key + j*j)
            j += 1


        return current % self.__numarMaxim


    def isEmpty(self):
        if self.numarElemente == 0:
            return True
        else:
            return False

    def add(self, elem):
        if self.isFull() == False:
            nume = elem.getNume()
            hash = self.hashCode(nume)
            if (self.tabela[hash] != None):
                hash = self.nextEmpty(hash)
            self.tabela[hash] = elem
            self.numarElemente += 1


    def size(self):
        return self.numarElemente

    def remove(self, nume):
        if self.search(nume) != None:
            hash = self.hashCode(nume)
            i = hash
            while (i <= self.__numarMaxim+hash):
                if self.tabela[i % self.__numarMaxim] != None and self.tabela[i % self.__numarMaxim].getNume() == nume:
                        self.tabela[i % self.__numarMaxim] = None
                        self.numarElemente -= 1
                i += 1

            # for i in range(self.__numarMaxim):
            #     if self.tabela[i] != None and self.tabela[i].getNume() == nume:
            #         self.tabela[i] = None
            #         self.numarElemente -= 1
