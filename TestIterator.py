import unittest
from TabelaDispersie import TabelaDispersie
from Iterator import Iterator
from Multime import Multime
from Movie import Movie


class TestIterator(unittest.TestCase):
    def testAll(self):
        self.testIterator()

    def testIterator(self):
        multime = Multime(10)
        iterator = Iterator(multime, 10)
        movie = Movie("nume", 9.9, "descriere", "gen")
        multime.add(movie)
        self.assertEqual(iterator.current(), None)
        iterator.next()
        self.assertEqual(iterator.current().getNume(), "nume")

        iterator.reset()
        self.assertEqual(iterator.current(), None)

    if __name__ == '__main__':
        unittest.main()