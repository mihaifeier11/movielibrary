import unittest
from TabelaDispersie import TabelaDispersie
from Movie import Movie
class TestTabela(unittest.TestCase):
    def testAll(self):
        self.testHashCode()
        self.testAddAndSearch()
        self.testIsFull()
        self.testRemove()
        self.testNextEmpty()

    def testHashCode(self):
        tabelaDispersie = TabelaDispersie(10)
        self.assertEqual(tabelaDispersie.hashCode("cba"), 7)

    def testIsFull(self):
        tabelaDispersie = TabelaDispersie(10)
        movie = Movie("nume", 9.9, "descriere", "gen")
        tabelaDispersie.add(movie)
        tabelaDispersie.add(movie)
        tabelaDispersie.add(movie)
        tabelaDispersie.add(movie)
        tabelaDispersie.add(movie)
        tabelaDispersie.add(movie)
        tabelaDispersie.add(movie)
        tabelaDispersie.add(movie)
        tabelaDispersie.add(movie)
        tabelaDispersie.add(movie)
        self.assertEqual(tabelaDispersie.isFull(), True)


    def testAddAndSearch(self):
        tabelaDispersie = TabelaDispersie(10)
        movie = Movie("nume", 9.9, "descriere", "gen")
        tabelaDispersie.add(movie)
        movie2 = tabelaDispersie.search("nume")

        self.assertEqual(movie.getNume(), movie2.getNume())
        self.assertEqual(movie.getRating(), movie2.getRating())
        self.assertEqual(movie.getDescriere(), movie2.getDescriere())
        self.assertEqual(movie.getGen(), movie2.getGen())

        self.assertEqual(tabelaDispersie.search("nue"), None)

    def testRemove(self):
        tabelaDispersie = TabelaDispersie(10)
        movie = Movie("nume", 9.9, "descriere", "gen")
        tabelaDispersie.add(movie)
        tabelaDispersie.add(movie)
        tabelaDispersie.add(movie)
        tabelaDispersie.add(movie)
        tabelaDispersie.add(movie)
        tabelaDispersie.add(movie)
        tabelaDispersie.add(movie)
        tabelaDispersie.add(movie)
        tabelaDispersie.add(movie)
        tabelaDispersie.add(movie)
        self.assertEqual(tabelaDispersie.isFull(), True)
        tabelaDispersie.remove("nume")
        self.assertEqual(tabelaDispersie.isFull(), False)

    def testNextEmpty(self):
        tabelaDispersie = TabelaDispersie(10)
        movie = Movie("nume", 9.9, "descriere", "gen")
        self.assertEqual(tabelaDispersie.nextEmpty(0), 0)
        tabelaDispersie.add(movie)
        self.assertEqual(tabelaDispersie.nextEmpty(1), 2)

    def testIsEmpty(self):
        tabelaDispersie = TabelaDispersie(10)
        movie = Movie("nume", 9.9, "descriere", "gen")
        self.assertEqual(tabelaDispersie.isEmpty(), True)

        tabelaDispersie.add(movie)
        self.assertEqual(tabelaDispersie.isEmpty(), False)

