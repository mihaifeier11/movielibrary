import unittest
from Movie import Movie
class TestMovie(unittest.TestCase):
    def testGet(self):
        self.__movie = Movie("nume", 9.9, "descriere", "gen")

        self.assertEqual(self.__movie.getNume(), "nume")
        self.assertEqual(self.__movie.getRating(), 9.9)
        self.assertEqual(self.__movie.getDescriere(), "descriere")
        self.assertEqual(self.__movie.getGen(), "gen")