import unittest
from Multime import Multime
from Movie import Movie
from Iterator import Iterator
from Service import Service

class TestService(unittest.TestCase):
    def testAddAndSearch(self):
        service = Service(10)
        service.add("nume", 9, "descriere", "gen")
        movie = service.search("nume")
        self.assertEqual(movie.getRating(), 9)
        self.assertEqual(movie.getDescriere(), "descriere")
        self.assertEqual(movie.getGen(), "gen")
        self.assertEqual(service.search("nume2"), False)

    def testRemove(self):
        service = Service(10)
        service.add("nume", 9, "descriere", "gen")
        movie = service.search("nume")
        self.assertEqual(movie.getRating(), 9)
        self.assertEqual(movie.getDescriere(), "descriere")
        self.assertEqual(movie.getGen(), "gen")
        service.remove("nume")
        self.assertEqual(service.search("nume2"), False)

    def testFilterRating(self):
        service = Service(10)
        service.add("nume", 9, "descriere", "gen")
        service.add("asd", 1, "descriere", "gen2")
        l = service.filterGen("gen")
        self.assertEqual(len(l), 1)
        self.assertEqual(l[0].getNume(), "nume")
        self.assertEqual(l[0].getRating(), 9)
        self.assertEqual(l[0].getDescriere(), "descriere")
        self.assertEqual(l[0].getGen(), "gen")


