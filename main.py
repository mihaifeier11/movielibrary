from TestMovie import TestMovie
from TestTabela import TestTabela
from Multime import Multime
from Iterator import Iterator
from Movie import Movie
from Ui import UI

testMovie = TestMovie()
testMovie.testGet()

testTabela = TestTabela()
testTabela.testAll()

ui = UI()
ui.run()