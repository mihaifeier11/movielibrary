from Service import Service

numarMaxim = 11

class UI:
    def __init__(self):
        self.__service = Service(numarMaxim)
        self.__service.readFromFile()

    def menu(self):
        print ("1. Adauga un film.")
        print ("2. Sterge un film.")
        print ("3. Cauta un film.")
        print ("4. Filtrare filme dupa rating.")
        print ("5. Filtrare filme dupa gen.")
        print ("0. Exit.")

    def adaugaFilm(self):
        nume = input("Nume: ")
        rating = float(input("Rating: "))
        descriere = input("Descriere: ")
        gen = input("Gen: ")
        self.__service.add(nume, rating, descriere, gen + "\n")

    def stergeFilm(self):
        nume = input("Nume: ")
        self.__service.remove(nume)

    def cautaFilm(self):
        nume = input("Nume: ")
        movie = self.__service.search(nume)
        if (movie == False):
            print ("Filmul nu exista")
        else:
            print ("Nume: " + movie.getNume())
            print ("Rating: " + str(movie.getRating()))
            print ("Descriere: " + movie.getDescriere())
            print ("Gen: " + movie.getGen())

    def filtrareRating(self):
        rating = float(input("Rating: "))
        movies = self.__service.filterRating(rating)
        if (len(movies) > 0):
            for movie in movies:
                print ("Nume: " + movie.getNume())
                print ("Rating: " + str(movie.getRating()))
                print ("Descriere: " + movie.getDescriere())
                print ("Gen: " + movie.getGen())
        else:
            print ("Nu exista niciun film cu acest criteriu. ")

    def filtrareGen(self):
        gen = input("Gen: ")
        movies = self.__service.filterGen(gen)
        if (len(movies) > 0):
            for movie in movies:
                print ("Nume: " + movie.getNume())
                print ("Rating: " + str(movie.getRating()))
                print ("Descriere: " + movie.getDescriere())
                print ("Gen: " + movie.getGen())
        else:
            print ("Nu exista niciun film cu acest criteriu. ")

    def run(self):
        self.menu()
        while True:
            command = input("Comanda: ")
            if (command == "1"):
                self.adaugaFilm()
            elif (command == "2"):
                self.stergeFilm()
            elif (command == "3"):
                self.cautaFilm()
            elif (command == "4"):
                self.filtrareRating()
            elif (command == "5"):
                self.filtrareGen()
            elif (command == "0"):
                self.__service.writeToFile()
                print ("O zi buna!")
                break;
            else:
                print ("Comanda invalida.")
